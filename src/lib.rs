#[macro_use]
extern crate serde_derive;

extern crate serde_json;

use std::fs::File;
use std::io::BufReader;

#[derive(Serialize, Deserialize, Debug)]
pub struct Obj {
    pub profile: Vec<Profile>,
    pub slides: Vec<Slides>,
}
 
#[derive(Serialize, Deserialize, Debug)]
pub struct Slides {
    pub id: String,
    pub file: String,
    pub title: String,
    pub description: String,
    pub theme: String,
    pub url: String,
}
 
#[derive(Serialize, Deserialize, Debug)]
pub struct Profile {
    pub name: String,
    pub profile_picture: String,
    pub bio: String,
    pub facebook: String,
    pub twitter: String,
    pub linkedin: String,
    pub github: String,
}

pub fn get_data() -> Result<Obj, Box<dyn std::error::Error>> {
    let file = File::open("./static/data/data.json")?;
    let reader = BufReader::new(file);

    let p = serde_json::from_reader(reader)?;

    Ok(p)
}

